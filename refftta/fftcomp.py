"""FFTComp class for computing FFT and storing the results.
"""

import timeit

import numpy as np

import config
import fu
import utils

class FFTComp:
	"""Computes FFT and stores all intermediate results in its parameters.

	Attributes
	----------
	Nexps : list of int
		List of Nexp values which will be computed. Each Nexp has its own set
		of results.
	Nexp_max : int
		Maximum Nexp read from a config file.
	nstages : dict of dict of int
		How many radix-4 and radix-2 stages are needed for a FFT size 2**Nexp.
		Stored as a dictionary for every Nexp with keys 'rdx2' and 'rdx4'.
	total_stages : dict of int
		Total number of stages needed for every Nexp.
	x_in : dict of arrays of complex
		Input vectors.
	x : dict of arrays of complex
		One x per Nexp. Vector for the in-place computation. In the beginning
		it loads values from x_in. Computed values are directly written to x,
		overwriting previous values.
	x_dump : dict of arrays of comples
		Saves the x vector in the every stage. It is useful for potential
		future debugging since it includes all computed values in all stages.
	y : dict of arrays of complex
		Computed final results of FFT after the output permutation
	y_ref : dict of arrays of complex
		Reference FFT results.
	addr : dict of arrays of np.uint16
		Computed addresses in all stages.
	k : dict of arrays of np.uint16
		Computed twiddle factor indexes in all stages.
	tf : dict of arays of complex
		Computed twiddle factors in all stages.
	scale : bool
		Tells whether to scale the cadd FU result. Applies to all Nexps.
	normalize : bool
		Tells whether to normalize the final result. Applies to all Nexps.
	tf_lut : array of complex
		Lookup table of twiddle factors. Computed for the max FFT size.
	"""

	def __init__(self):
		"""Empty attributes allocation.
		"""

		self.Nexps = []
		self.Nexp_max = int()
		self.nstages = {}
		self.total_stages = {}
		self.x_in = {}
		self.x = {}
		self.x_dump = {}
		self.y = {}
		self.y_ref = {}
		self.addr = {}
		self.k = {}
		self.tf = {}
		self.scale = bool()
		self.normalize = bool()
		self.tf_lut = np.array([], complex)

	def setup(self, x_in, Nexps, scale=False, normalize=False):
		"""Assign values to attributes.
		"""

		self.x_in = x_in
		self.Nexps = Nexps
		self.Nexp_max = int(config.cp['Limit_values']['nexp_max'])
		self.scale = scale
		self.normalize = normalize
		self.tf_lut = fu.tf_lookup(self.Nexp_max)
		for Nexp in self.Nexps:
			N = 2**Nexp
			self.nstages[Nexp] = fu.stages_needed(Nexp)
			self.total_stages[Nexp] = np.sum(list(self.nstages[Nexp].values()))
			self.addr[Nexp] = np.zeros((N, self.total_stages[Nexp]), np.uint16)
			self.k[Nexp] = np.zeros((N, self.total_stages[Nexp]), np.uint16)
			self.tf[Nexp] = np.zeros((N, self.total_stages[Nexp]), complex)
			self.x_dump[Nexp] = np.zeros((N, self.total_stages[Nexp]), complex)
			self.x[Nexp] = np.zeros(N, complex)
			self.y[Nexp] = np.zeros(N, complex)

	def compute_all(self):
		"""Compute FFTs for all Nexps with errors.
		"""

		for Nexp in self.Nexps:
			print("-"*79)
			start = timeit.default_timer()
			self.x[Nexp] = np.array(self.x_in[Nexp])
			self.print_header(Nexp)
			self.main_loop(Nexp)
			if self.normalize is True:
				maxval = np.max(np.abs([ np.real(self.x[Nexp]),
				                         np.imag(self.x[Nexp]) ]))
				print("\nNormalizing... division by {:.3f}\n".format(maxval))
				self.x[Nexp] = np.divide(self.x[Nexp], maxval)
			print("\nOutput permutation...\n")
			self.out_permutation(Nexp)
			err = self.errors(Nexp)
			print("Mean square error: {}".format(err['mse']))
			print("Relative error:    {}".format(err['rel']))
			end = timeit.default_timer()
			print("\nDone. Computation time: {:.5f} s".format(end-start))

	def compute_some(self, what, prints=True):
		"""Compute just some required outputs.
		"""

		if prints:
			print("Computing for Nexp:", end='', flush=True)
		for Nexp in self.Nexps:
			if prints:
				print(" {}".format(Nexp), end='', flush=True)
			N = 2**Nexp
			if what in ['out']:
				self.x[Nexp] = np.array(self.x_in[Nexp])
			for stage in range(self.total_stages[Nexp]):
				rdx2_flag = self.rdx2_stage(stage, Nexp)
				for lin_idx in range(0, N, 4):
					P    = np.zeros(4, complex)    # Input for the butterfly
					addr = np.zeros(4, np.uint16)  # Addresses for butterfly
					K    = np.zeros(4, np.uint16)  # Twiddle factor coefs (k)
					tf   = np.zeros(4, complex)    # Twiddle factors
					X    = np.zeros(4, complex)    # Input
					if what in ['ag', 'out']:
						addr = fu.ag(lin_idx, stage, Nexp)
						self.addr[Nexp][lin_idx:lin_idx+4, stage] = addr
						for i in range(4):
							X[i] = self.x[Nexp][addr[i]]
					if what in ['tfg_k', 'tfg_tf', 'out']:
						K = fu.tf_k(lin_idx, stage, Nexp, rdx2_flag)
						self.k[Nexp][lin_idx:lin_idx+4, stage] = K
					if what in ['tfg_tf', 'out']:
						tf = fu.tfg(K, self.tf_lut, self.Nexp_max)
						self.tf[Nexp][lin_idx:lin_idx+4, stage] = tf
					if what in ['out']:
						P = np.multiply(tf, X)
						Y = fu.cadd(P, rdx2_flag, self.scale)
						for i in range(4):
							self.x[Nexp][addr[i]] = Y[i]
			if what in ['out']:
				if self.normalize is True:
					maxval = np.max(np.abs([ np.real(self.x[Nexp]),
					                         np.imag(self.x[Nexp]) ]))
					self.x[Nexp] = np.divide(self.x[Nexp], maxval)
			if prints:
				print(";", end='', flush=True)
		print("")

	def main_loop(self, Nexp):
		"""Main computation loop. Roughly follows the intended TTA schedule.
		"""

		N = 2**Nexp
		for stage in range(self.total_stages[Nexp]):
			rdx2_flag = self.rdx2_stage(stage, Nexp)
			if rdx2_flag:
				print("Computing stage {} (radix-2)...".format(stage))
			else:
				print("Computing stage {} (radix-4)...".format(stage))
			for lin_idx in range(0, N, 4):
				# Variables
				P    = np.zeros(4, complex)    # Input for the butterfly
				addr = np.zeros(4, np.uint16)  # Addresses for butterfly
				K    = np.zeros(4, np.uint16)  # Twiddle factor coefs (k)
				tf   = np.zeros(4, complex)    # Twiddle factors
				X    = np.zeros(4, complex)    # Input
				# Main computation
				addr = fu.ag(lin_idx, stage, Nexp)
				for i in range(4):
					X[i] = self.x[Nexp][addr[i]]
				K  = fu.tf_k(lin_idx, stage, Nexp, rdx2_flag)
				tf = fu.tfg(K, self.tf_lut, self.Nexp_max)
				P  = np.multiply(tf, X)
				Y  = fu.cadd(P, rdx2_flag, self.scale)
				# Swap
				for i in range(4):
					self.x[Nexp][addr[i]] = Y[i]
				# Printing and debug
				self.addr[Nexp][lin_idx:lin_idx+4, stage] = addr
				self.k[Nexp][lin_idx:lin_idx+4, stage] = K
				self.tf[Nexp][lin_idx:lin_idx+4, stage] = tf
			self.x_dump[Nexp][:, stage] = self.x[Nexp]

	def rdx2_stage(self, stage, Nexp):
		"""Is the current stage a radix-2 stage?
		"""

		rdx2 = bool(self.nstages[Nexp]['rdx2'])
		last_stage = (stage == self.total_stages[Nexp]-1)
		if rdx2 and last_stage:
			return True
		else:
			return False

	def out_permutation(self, Nexp):
		"""Last stage of the computation. Put the results into a correct order.

		The output permutation is done by splitting the output value addresses
		into bit pairs and reversing them.
		"""

		N = 2**Nexp
		for i in range(0, N):
			idx = utils.bit_pair_reverse(i, Nexp)
			self.y[Nexp][i] = self.x[Nexp][idx]

	def print_header(self, Nexp):
		"""Print initial computation information.
		"""

		print("FFT size: N = {} (2^{})".format(2**Nexp, Nexp))
		print("Twiddle factor LUT size is {} (using k-scaling)."
		        .format(len(self.tf_lut)))
		print("Stages needed:")
		print("  radix-4: {}".format(self.nstages[Nexp]['rdx4']))
		print("  radix-2: {}".format(self.nstages[Nexp]['rdx2']))
		if self.scale is True:
			scale_msg = "scaled"
		else:
			scale_msg = "not scaled"
		if self.normalize is True:
			norm_msg = "normalized"
		else:
			norm_msg = "not normalized"
		print("Output is *{}* and *{}*.".format(scale_msg, norm_msg))
		print("")

	def errors(self, Nexp):
		"""Reference and error computation.

		Contains mean square error (MSE) and a relative error.
		"""

		N = 2**Nexp
		self.y_ref[Nexp] = np.fft.fft(self.x_in[Nexp], N)
		mse = utils.mse(self.y[Nexp], self.y_ref[Nexp])
		rel_err = utils.rel_err(self.y[Nexp], self.y_ref[Nexp])
		err = {'mse': mse, 'rel': rel_err}
		return err

	def print_idxs(self, Nexp):
		"""Print operand addresses and twiddle factor indexes for each stage.
		"""

		N = 2**Nexp
		str0 = ['']
		str1 = ['i  |']
		str2 = ['----']
		for i in range(0, self.total_stages[Nexp]):
			str0.append("        s{}".format(i))
			str1.append(" ad   k  |")
			str2.append("----------")
		print(''.join( str0))
		print(''.join( str1))
		print(''.join( str2))

		num = np.zeros((N, int(2*self.total_stages[Nexp]+1)), np.uint16)
		for i in range(N):
			row = i
			for k in range(0, self.total_stages[Nexp]):
				row = np.append(row, self.addr[Nexp][i,k])
				row = np.append(row, self.k[Nexp][i,k])
			num[i,:] = row
		for row in num:
			print("".join("{}".format(row[i])
			  .ljust(5) for i in range(num.shape[1])))

	def compare_results(self, Nexp):
		"""Compare the computed result with a reference.

		Last column is a computed result before the output permutation.

		See Also
		--------
		numpy.fft.fft : Used for reference computation.
		"""

		N = 2**Nexp
		print(" i             in     reference      computed      no perm.")
		print("-----------------------------------------------------------")
		for i in range(N):
			num = [ self.x_in[Nexp][i], self.y_ref[Nexp][i],
			        self.y[Nexp][i], self.x[Nexp][i] ]
			#print("{0}\t{1:.1f}\t{2:.1f}\t{3:.1f}\t{4:.1f}"
			#  .format(i, x_in[i], y_ref[i], y[i], x[i]))
			print('{:2d}'.format(i), ''.join('{:.1f}'
			      .format(num[n]).rjust(14) for n in range(len(num))))
