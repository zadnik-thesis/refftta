# refftta/input #

Input values for the FFT computation. Each input group has its own folder. You
can generate your own inputs, put them into a custom folders and put that folder
name into 'config.txt'. This allows you to load your own values.

The input files are .csv files and need to be named inNexp.csv, where Nexp is
the FFT size as a power of 2 (N = 2^Nexp).

[//]: # (tree)
## Directory tree ##
~~~~
/home/kubouch/git/thesis/refftta/refftta/input
└── randcplx  // Random complex numbers
~~~~
