#!/usr/bin/env python3
"""
First entry point.

Called by 'python refftta [options]' from the root directory. If you want to
keep variables in your namespace, call core.py directly.
"""

import sys
import core

def main():
	sys.exit(core.main())

if __name__ == "__main__":
	main()
