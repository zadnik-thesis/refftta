"""
Reference data and files generation.
"""

import numpy as np

import config
import fu
import utils

def create_refs(ref_types, fftComp):
	"""Create desired reference files.
	"""

	for ref_type in ref_types:
		print("Generating reference files for *{}*!".format(ref_type))
		ref_name = ref_name_parse(ref_type, fftComp)
		data_dict = ref_data(ref_type, fftComp)
		ref_files(ref_name, data_dict)
		print("")

def ref_data(ref_type, fftComp):
	"""Generate reference data
	"""

	# Import is here since the fpconv path is set up later than importing ref
	import fpconv
	if ref_type == 'cadd':
		return fpconv.cplx2fixed(tst_cadd())
	elif ref_type == 'inp':
		data_dict = dict(fftComp.x_in)
	elif ref_type == 'out':
		if all_zeros(fftComp.x):
			fftComp.compute_some('out')
		data_dict = dict(fftComp.x)
	elif ref_type == 'tfg_tf':
		if all_zeros(fftComp.tf):
			fftComp.compute_some('tfg_tf')
		data_dict = dict(fftComp.tf)
	elif ref_type == 'tfg_k':
		if all_zeros(fftComp.k):
			fftComp.compute_some('tfg_k')
		data_dict = dict(fftComp.k)
	elif ref_type == 'ag':
		if all_zeros(fftComp.addr):
			fftComp.compute_some('ag')
		data_dict = dict(fftComp.addr)
	# Complex to fixed point conversion if needed
	for Nexp in data_dict.keys():
		if 'complex' in data_dict[Nexp].dtype.name:
			data_dict[Nexp] = fpconv.cplx2fixed(data_dict[Nexp])
	return data_dict

def ref_files(ref_name, data_dict):
	"""Create reference files from given data.
	"""

	try:
		for Nexp in data_dict.keys():
			ref_file = ref_file_path(ref_name, Nexp)
			print("{}".format(str(ref_file)), flush=True)
			try:
				data = np.concatenate([col for col in data_dict[Nexp].T])
			except ValueError:
				data = data_dict[Nexp]
			file_write_fp(data, ref_file)
	except AttributeError:
		ref_file = ref_file_path(ref_name)
		print("{}".format(str(ref_file)), flush=True)
		data = data_dict
		file_write_fp(data, ref_file)

def ref_name_parse(ref_type, fftComp):
	"""Special file naming can be added here if needed.
	"""

	if ref_type == 'out' and fftComp.normalize:
		return 'outn'
	elif ref_type == 'cadd':
		return 'cadd1'
	else:
		return ref_type

def ref_file_path(ref_name, Nexp=None, ensure=True):
	"""Figure out the path of reference files.
	"""

	if Nexp:
		ref_file = config.DIRS['PY_REF'] \
		                    .joinpath(ref_name) \
		                    .joinpath("ref_{}_{:02d}".format(ref_name, Nexp))
	else:
		ref_file = config.DIRS['PY_REF'] \
		                    .joinpath(ref_name) \
		                    .joinpath("ref_{}".format(ref_name))
	if ensure:
		utils.ensure_dirs(ref_file, path_is_file=True)
	return ref_file

def file_write_fp(array, filename):
	"""Write an array of values into filename in a hexadecimal format.

	Array must be one-dimensional.
	"""

	if not isinstance(array, np.ndarray):
		array = np.array(array)
	if len(array.shape) > 1:
		raise ValueError("Array must be one-dimensional.")

	with open(filename, 'w') as wf:
		for value in array:
			wf.write("{:#010x}\n".format(value))

def all_zeros(array_dict):
	"""Check whether all values of a dictionary are zeros.
	"""

	try:
		return bool(sum(not array.any() for array in array_dict.values()))
	except TypeError:
		raise TypeError("Wrong input type (needs dict).")

def tst_cadd(P = None):
	"""Testing function for a complex adder.
	"""

	if (P == None) or (len(P) != 4):
		P = np.array([ 0.13227177-0.92608986j,
		               0.96191367+0.11362414j,
		               0.43124304+0.24827742j,
		               0.46915433-0.5619068j ])
	# Real parts (rdx2)
	res = np.array(fu.cadd(np.real(P), rdx2_flag=True, scale=True))
	# Imag parts (rdx2)
	res = np.append([res], [ fu.cadd(np.multiply(np.imag(P), 1j),
	                                 rdx2_flag=True,
	                                 scale=True)
	                       ], axis=0)
	# Complex (rdx2)
	res = np.append(res, [ fu.cadd(P, rdx2_flag=True, scale=True) ], axis=0)
	# Real parts (rdx4)
	res = np.append(res, [ fu.cadd(np.real(P),
	                               rdx2_flag=False,
	                               scale=True)
	                     ], axis=0)
	# Imag parts (rdx4)
	res = np.append(res, [ fu.cadd(np.multiply(np.imag(P), 1j),
	                               rdx2_flag=False,
	                               scale=True)
	                     ], axis=0)
	# Complex (rdx4)
	res = np.append(res, [ fu.cadd(P, rdx2_flag=False, scale=True) ], axis=0)
	return np.concatenate(res)
